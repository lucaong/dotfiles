# Dotfiles

Initialize/update my Mac computer(s) to my preferred setup, installing software
and applying configuration


# Installation

```
cd ~ && git clone https://lucaong@bitbucket.org/lucaong/dotfiles.git ~/.dotfiles && cd ~/.dotfiles
./setup
```


# Updating

```
cd ~/.dotfiles
git pull
./setup
```

# Options

```
./setup --help
```
